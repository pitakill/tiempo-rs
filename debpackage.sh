COPYRIGHT_YEARS="2018 - "$(date "+%Y")
DPKG_STAGING="debian-package"
DPKG_DIR="${DPKG_STAGING}/dpkg"

PROJECT_MANTAINER="Abraham Toriz Cruz"
PROJECT_HOMEPAGE="https://categulario.gitlab.io/tiempo/en"
PROJECT_NAME=tiempo
PROJECT_VERSION=${CI_COMMIT_TAG:1}
PROJECT_BINARY=t

mkdir -p "${DPKG_DIR}"

DPKG_BASENAME=${PROJECT_NAME}
DPKG_CONFLICTS=
DPKG_VERSION=${PROJECT_VERSION}
DPKG_ARCH=amd64
DPKG_NAME="${DPKG_BASENAME}_${DPKG_VERSION}_${DPKG_ARCH}.deb"

# Binary
install -Dm755 "target/release/$PROJECT_BINARY" "${DPKG_DIR}/usr/bin/$PROJECT_BINARY"
# README and LICENSE
install -Dm644 "README.md" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/README.md"
install -Dm644 "LICENSE" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/LICENSE"
install -Dm644 "CHANGELOG.md" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/changelog"
gzip -n --best "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/changelog"

cat > "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/copyright" <<EOF
Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: $PROJECT_NAME
Source: $PROJECT_HOMEPAGE
Files: *
Copyright: $PROJECT_MANTAINER
Copyright: $COPYRIGHT_YEARS $PROJECT_MANTAINER
License: GPL
EOF

chmod 644 "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/copyright"

# control file
mkdir -p "${DPKG_DIR}/DEBIAN"
cat > "${DPKG_DIR}/DEBIAN/control" <<EOF
Package: ${DPKG_BASENAME}
Version: ${DPKG_VERSION}
Section: graphics
Priority: optional
Maintainer: ${PROJECT_MANTAINER}
Homepage: ${PROJECT_HOMEPAGE}
Architecture: ${DPKG_ARCH}
Provides: ${PROJECT_NAME}
Depends: libgtk-3-0
Conflicts: ${DPKG_CONFLICTS}
Description: A simple infinite-canvas free-hand vector drawing application
  A simple infinite-canvas free-hand vector drawing application
EOF
DPKG_PATH="${DPKG_STAGING}/${DPKG_NAME}"
# build dpkg
fakeroot dpkg-deb --build "${DPKG_DIR}" "${DPKG_PATH}"
