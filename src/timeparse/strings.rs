use std::collections::HashMap;

use regex::Regex;

lazy_static! {
    // https://regex101.com/r/V9zYQu/1/
    pub static ref HUMAN_REGEX: Regex = Regex::new(r"(?xi)
(?P<hour>
    (?P<hnum>
        (?P<half>half(\s+)a(n)?)|
        (?P<hcase>a|an|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen)|
        (?P<hdigit>one|two|three|four|five|six|seven|eight|nine)|
        (?P<hten>ten|twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)|
        (?P<hcomposed>
            (?P<hcten>ten|twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)
            .(?P<hcdigit>one|two|three|four|five|six|seven|eight|nine)
        )|
        (?P<htextualnum>\d+)
    )
    (\s+)?h(ou)?(r)?s?
)?
(?P<sep>\s*(,|and)?\s+)?
(?P<minute>
    (?P<mnum>
        (?P<mcase>a|an|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen)|
        (?P<mdigit>one|two|three|four|five|six|seven|eight|nine)|
        (?P<mten>ten|twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)|
        (?P<mcomposed>
            (?P<mcten>ten|twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)
            .(?P<mcdigit>one|two|three|four|five|six|seven|eight|nine)
        )|
        (?P<mtextualnum>\d+)
    )
    (\s+)?m(in)?(ute?)?s?
)?
\s+ago
    ").unwrap();

    pub static ref DATETIME_REGEX: Regex = Regex::new(r"(?xi)
        (?P<year>\d{4})  # the year, mandatory
        .
        (?P<month>\d{2}) # the month, mandatory
        .
        (?P<day>\d{2})   # the day, mandatory
        (. # a separator
        (?P<hour>\d{2}) # the hour, optional
        (. # a separator
        (?P<minute>\d{2})? # the minute, optional
        (. # a separator
        (?P<second>\d{2}))?)?)? # the second, optional, implies minute
        (?P<offset>
            (?P<utc>Z)|((?P<sign>\+|-)(?P<ohour>\d{1,2}):(?P<omin>\d{2}))
        )? # the offset, optional
    ").unwrap();

    pub static ref HOUR_REGEX: Regex = Regex::new(r"(?xi)
        (?P<hour>\d{1,2}) # the hour, mandatory
        (. # a separator
        (?P<minute>\d{2})? # the minute, optional
        (. # a separator
        (?P<second>\d{2}))?)? # the second, optional, implies minute
        (?P<offset>
            (?P<utc>Z)|((?P<sign>\+|-)(?P<ohour>\d{1,2}):(?P<omin>\d{2}))
        )? # the offset, optional
    ").unwrap();

    pub static ref NUMBER_VALUES: HashMap<&'static str, f64> = {
        vec![
            ("a", 1.),
            ("an", 1.),
            ("ten", 10.),
            ("eleven", 11.),
            ("twelve", 12.),
            ("thirteen", 13.),
            ("fourteen", 14.),
            ("fifteen", 15.),
            ("sixteen", 16.),
            ("seventeen", 17.),
            ("eighteen", 18.),
            ("nineteen", 19.),

            ("one", 1.),
            ("two", 2.),
            ("three", 3.),
            ("four", 4.),
            ("five", 5.),
            ("six", 6.),
            ("seven", 7.),
            ("eight", 8.),
            ("nine", 9.),

            ("twenty", 20.),
            ("thirty", 30.),
            ("forty", 40.),
            ("fifty", 50.),
            ("sixty", 60.),
            ("seventy", 70.),
            ("eighty", 80.),
            ("ninety", 90.),
        ].into_iter().collect()
    };
}
