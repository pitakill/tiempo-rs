use std::fmt;

#[derive(PartialEq, Eq)]
pub struct Ps<'a>(pub &'a str);

/// Make diff to display string as multi-line string
impl<'a> fmt::Debug for Ps<'a> {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.write_str(self.0)
  }
}
