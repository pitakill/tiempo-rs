use std::io::Write;

use crate::models::Entry;
use crate::error::Result;

pub fn print_formatted<W: Write>(entries: Vec<Entry>, out: &mut W) -> Result<()> {
    Ok(writeln!(out, "{}", entries.into_iter().map(|e| e.id.to_string()).collect::<Vec<_>>().join(" "))?)
}
