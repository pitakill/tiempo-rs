use std::io::{BufRead, Write};

use crate::database::{Database, SqliteDatabase};

pub struct Streams<D, I, O, E>
where
    D: Database,
    I: BufRead,
    O: Write,
    E: Write,
{
    pub db: D,
    pub r#in: I,
    pub out: O,
    pub err: E,
}

impl Streams<SqliteDatabase, &[u8], Vec<u8>, Vec<u8>> {
    pub fn fake(r#in: &[u8]) -> Streams<SqliteDatabase, &[u8], Vec<u8>, Vec<u8>> {
        let mut db = SqliteDatabase::from_memory().unwrap();

        db.init().unwrap();

        Streams {
            db,
            r#in,
            out: Vec::new(),
            err: Vec::new(),
        }
    }

    pub fn fake_old(r#in: &[u8]) -> Streams<SqliteDatabase, &[u8], Vec<u8>, Vec<u8>> {
        let mut db = SqliteDatabase::from_memory().unwrap();

        db.init_old().unwrap();

        Streams {
            db,
            r#in,
            out: Vec::new(),
            err: Vec::new(),
        }
    }

    pub fn with_db(self, db: SqliteDatabase) -> Self {
        Streams {
            db,
            ..self
        }
    }

    pub fn reset_io(&mut self) {
        self.out = Vec::new();
        self.err = Vec::new();
    }
}
