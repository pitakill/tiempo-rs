use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{DateTime, Utc};

use crate::error::Result;
use crate::database::Database;
use crate::config::Config;
use crate::io::Streams;
use crate::env::Env;

pub mod r#in;
pub mod display;
pub mod today;
pub mod yesterday;
pub mod sheet;
pub mod week;
pub mod month;
pub mod list;
pub mod out;
pub mod resume;
pub mod backend;
pub mod kill;
pub mod now;
pub mod edit;
pub mod archive;
pub mod configure;

pub struct Facts {
    pub now: DateTime<Utc>,
    pub config: Config,
    pub env: Env,
}

impl Facts {
    pub fn new() -> Facts {
        Facts {
            now: Utc::now(),
            config: Default::default(),
            env: Default::default(),
        }
    }

    pub fn with_config(self, config: Config) -> Facts {
        Facts {
            config,
            ..self
        }
    }

    pub fn with_now(self, now: DateTime<Utc>) -> Facts {
        Facts {
            now,
            ..self
        }
    }
}

impl Default for Facts {
    fn default() -> Facts {
        Facts::new()
    }
}

pub trait Command<'a> {
    type Args: TryFrom<&'a ArgMatches<'a>>;

    fn handle<D: Database, I: BufRead, O: Write, E: Write>(args: Self::Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>;
}
