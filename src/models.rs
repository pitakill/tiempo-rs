use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Entry {
    pub id: u64,
    pub note: Option<String>,
    pub start: DateTime<Utc>,
    pub end: Option<DateTime<Utc>>,
    pub sheet: String,
}

#[derive(Debug)]
pub struct Meta {
    pub id: u64,
    pub key: String,
    pub value: String,
}

impl Entry {
    #[cfg(test)]
    pub fn new_sample(id: u64, start: DateTime<Utc>, end: Option<DateTime<Utc>>) -> Entry {
        Entry {
            id,
            note: Some(format!("entry {}", id)),
            start, end,
            sheet: "default".into(),
        }
    }
}
