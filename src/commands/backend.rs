use std::process::{Command, Stdio};

use crate::error::Result;
use crate::config::Config;
use crate::error::Error::*;

pub struct BackendCommand;

impl BackendCommand {
    pub fn handle(config: &Config) -> Result<()> {
        let status = Command::new("sqlite3")
            .arg(&config.database_file)
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .output().map_err(Sqlite3CommandFailed)?
            .status;

        if status.success() {
            Ok(())
        } else {
            Err(Sqlite3CommandFailedUnkown)
        }
    }
}
