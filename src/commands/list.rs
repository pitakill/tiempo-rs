use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{Utc, Duration, Local};
use itertools::Itertools;
use ansi_term::Style;

use crate::error::{Error, Result};
use crate::database::Database;
use crate::tabulate::{Tabulate, Col, Align::*};
use crate::formatters::text::format_duration;
use crate::models::Entry;
use crate::old::{entries_or_warning, warn_if_needed};
use crate::io::Streams;

use super::{Command, Facts};

#[derive(Default)]
pub struct Args {
    all: bool,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &ArgMatches) -> Result<Args> {
        Ok(Args {
            all: matches.is_present("all"),
        })
    }
}

pub struct ListCommand {}

impl<'a> Command<'a> for ListCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let today = facts.now.with_timezone(&Local).date().and_hms(0, 0, 0).with_timezone(&Utc);
        let entries = if args.all {
            streams.db.entries_full(None, None)?
        } else {
            streams.db.entries_all_visible(None, None)?
        };

        let (mut entries, needs_warning) = entries_or_warning(entries, &streams.db)?;

        let current = streams.db.current_sheet()?;
        let last = streams.db.last_sheet()?;

        // introducte two fake entries to make both current and last show up
        entries.push(Entry {
            id: 1, sheet: current.clone(), start: facts.now, end: Some(facts.now), note: None,
        });

        entries.sort_unstable_by_key(|e| e.sheet.clone());

        let mut total_running = Duration::seconds(0);
        let mut total_today = Duration::seconds(0);
        let mut total = Duration::seconds(0);

        let sheets: Vec<_> = entries
            .into_iter()
            .group_by(|e| e.sheet.clone())
            .into_iter()
            .map(|(key, group)| {
                let entries: Vec<_> = group.into_iter().collect();
                let s_running = facts.now - entries.iter().find(|e| e.end.is_none()).map(|e| e.start).unwrap_or(facts.now);
                let s_today = entries.iter().filter(|e| e.start > today).fold(Duration::seconds(0), |acc, e| {
                    acc + (e.end.unwrap_or(facts.now) - e.start)
                });
                let s_total = entries.into_iter().fold(Duration::seconds(0), |acc, e| {
                    acc + (e.end.unwrap_or(facts.now) - e.start)
                });

                total_running = total_running + s_running;
                total_today = total_today + s_today;
                total = total + s_total;

                (
                    if current == key {
                        "*".into()
                    } else if last.as_ref() == Some(&key) {
                        "-".into()
                    } else {
                        "".into()
                    },

                    key,

                    format_duration(s_running),

                    format_duration(s_today),

                    format_duration(s_total),
                )
            })
            .collect();

        let mut tabs = Tabulate::with_columns(vec![
            // indicator of current or prev sheet
            Col::min_width(1).and_alignment(Right),

            // sheet name
            Col::min_width(9).and_alignment(Left),

            // running time
            Col::min_width(9).and_alignment(Right)
                .color_if(Style::new().dimmed(), |s| s == "0:00:00")
                .color_if(Style::new().bold(), |s| s != "0:00:00"),

            // today
            Col::min_width(9).and_alignment(Right)
                .color_if(Style::new().dimmed(), |s| s == "0:00:00")
                .color_if(Style::new().bold(), |s| s != "0:00:00"),

            // accumulated
            Col::min_width(12).and_alignment(Right),
        ]);

        tabs.feed(vec!["".into(), "Timesheet".into(), "Running".into(), "Today".into(), "Total Time".into()]);
        tabs.separator(' ');

        for sheet in sheets {
            tabs.feed(vec![sheet.0, sheet.1, sheet.2, sheet.3, sheet.4]);
        }

        tabs.separator('-');

        tabs.feed(vec![
            "".into(),
            "".into(),
            format_duration(total_running),
            format_duration(total_today),
            format_duration(total),
        ]);

        streams.out.write_all(tabs.print(facts.env.stdout_is_tty).as_bytes())?;

        warn_if_needed(&mut streams.err, needs_warning, &facts.env)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use chrono::{Utc, TimeZone};
    use pretty_assertions::assert_eq;

    use crate::database::{SqliteDatabase, Database};
    use crate::test_utils::Ps;

    use super::*;

    #[test]
    fn list_sheets() {
        std::env::set_var("TZ", "CST+6");

        let args = Default::default();
        let mut streams = Streams::fake(b"");

        streams.db.set_current_sheet("sheet2").unwrap();
        streams.db.set_last_sheet("sheet4").unwrap();

        streams.db.entry_insert(Utc.ymd(2021, 1, 1).and_hms(0, 0, 0), Some(Utc.ymd(2021, 1, 1).and_hms(1, 0, 0)), None, "_archived").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 1, 1).and_hms(0, 0, 0), Some(Utc.ymd(2021, 1, 1).and_hms(10,13, 55)), None, "sheet1").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 1, 1).and_hms(0, 0, 0), Some(Utc.ymd(2021, 1, 1).and_hms(7, 39, 18)), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 1, 1).and_hms(12, 0, 0), Some(Utc.ymd(2021, 1, 1).and_hms(13, 52, 45)), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 1, 1).and_hms(12, 0, 0), None, None, "sheet4").unwrap();

        let now = Utc.ymd(2021, 1, 1).and_hms(13, 52, 45);
        let facts = Facts::new().with_now(now);

        ListCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("  Timesheet   Running     Today   Total Time

  sheet1      0:00:00   0:00:00     10:13:55
* sheet2      0:00:00   0:00:00      0:00:00
  sheet3      0:00:00   1:52:45      9:32:03
- sheet4      1:52:45   1:52:45      1:52:45
--------------------------------------------
              1:52:45   3:45:30     21:38:43
"));

        // now show all the sheets
        streams.reset_io();

        let args = Args {
            all: true,
        };

        ListCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("  Timesheet   Running     Today   Total Time

  _archived   0:00:00   0:00:00      1:00:00
  sheet1      0:00:00   0:00:00     10:13:55
* sheet2      0:00:00   0:00:00      0:00:00
  sheet3      0:00:00   1:52:45      9:32:03
- sheet4      1:52:45   1:52:45      1:52:45
--------------------------------------------
              1:52:45   3:45:30     22:38:43
"));
    }

    #[test]
    fn old_database() {
        let args = Default::default();
        let mut streams = Streams::fake(b"").with_db(
            SqliteDatabase::from_path("assets/test_list_old_database.db").unwrap()
        );

        let now = Local.ymd(2021, 7, 16).and_hms(11, 30, 45);
        let facts = Facts::new().with_now(now.with_timezone(&Utc));

        ListCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("  Timesheet   Running     Today   Total Time

* default     0:10:24   0:10:26      0:10:26
--------------------------------------------
              0:10:24   0:10:26      0:10:26
"));

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            "[WARNING] You are using the old timetrap format, it is advised that you update your database using t migrate\n"
        );
    }
}
