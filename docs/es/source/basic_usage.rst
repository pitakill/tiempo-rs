Introducción
============

¿Qué es Tiempo?
---------------

Tiempo es una herramienta de línea de comandos para rastrear el tiempo. Te ayuda a grabar
el tiempo empleado en diferentes actividades y de manera opcional te permite organizarlas
en proyectos o `sheets` (hojas). Tiempo es compatible con `Timetrap`_, su predecesor.

    ¿Por qué otro rastreador de tiempo en lugar de mejorar Timetrap?

* Timetrap es `difícil de instalar`_, difícil de mantenerlo `actualizado`_ (debido a Ruby).
  Con Tiempo puedes obtener o compilar un binario, ponerlo donde sea y simplemente funcionará
  en la máquina. Estoy embebiendo SQLite.
* Timetrap es lento (no hay modo de sacarle la vuelta, es por Ruby), algunos comandos
  tardan hasta un segundo. Tiempo siempre se siente rápido.
* Timetrap necesitaba una refactorización mayor para arreglar un problema con el huso
  horario (en un lenguaje en el que no soy experto). Estaba al tanto de este problema
  y diseñé Tiempo para guardar marcas de tiempo en UTC así como es posible
  trabajar con la base de datos hecha por Timetrap sin estropearla. Y muchas pruebas
  se han realizado que respaldan esta afirmación.

Tiempo tiene otras ventajas:

* Arreglé algunas inconsistencias en el input.
* Resolví algunas incidencias antiguas de Timetrap.
* Las columnas del output están siempre alineadas.
* La interfaz de línea de comandos es más fácil de descubrir (pregunta ``-h`` para cualquier subcomando).
* Los tiempos de conclusión se imprimen con un +1d para indicar que la actividad acabó al siguiente
  día.

Instalación
-----------

Si tienes instaladas las herramientas de `Rust`_, solo ejecuta:

.. code:: bash

    cargo install tiempo

Si usas Arch Linux, instala el paquete `tiempo-git` desde `AUR`_, por ejemplo:

.. code:: bash

   git clone https://aur.archlinux.org/tiempo-git.git && cd tiempo-git && makepkg -si

.. NOTE::
   Requieres tener instalado `Rust`_ y `Cargo`_. Para más información haz `clic aquí`_.

Inicio rápido
-------------

TODO

.. _Timetrap: https://github.com/samg/timetrap/
.. _difícil de instalar: https://github.com/samg/timetrap/issues/176
.. _actualizado: https://github.com/samg/timetrap/issues/174
.. _clic aquí: https://doc.rust-lang.org/cargo/getting-started/installation.html
.. _Rust: https://rust-lang.org
.. _Cargo: https://doc.rust-lang.org/book/ch01-03-hello-cargo.html
.. _AUR: https://aur.archlinux.org/packages/tiempo-git/
