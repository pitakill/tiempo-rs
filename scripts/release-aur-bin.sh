#!/bin/bash

# some useful variables
VERSION=${CI_COMMIT_TAG:1}
PROJECT_NAME=tiempo
PROJECT_BINARY=t
ARCHIVENAME=$PROJECT_NAME-$VERSION-x86_64.tar.gz

# clone the repo
git clone $BIN_REPO_URL $PROJECT_NAME-bin

# enter it
cd $PROJECT_NAME-bin

# get the sum from the artifacts
SUM=( `cat ../$ARCHIVENAME.sum` )

# Generate the PKGBUILD
echo "# Maintainer: Abraham Toriz <categulario at gmail dot com>
pkgname=$PROJECT_NAME-bin
pkgver=$VERSION
pkgrel=1
pkgdesc='A free-hand vector drawing application with infinite canvas'
arch=('x86_64')
url='https://gitlab.com/categulario/tiempo-rs'
license=('GPL3')
depends=('gtk3')
provides=('$PROJECT_NAME')
conflicts=('$PROJECT_NAME')
source=(\"https://$PROJECT_NAME.categulario.tk/releases/any-linux/$PROJECT_NAME-\$pkgver-\$arch.tar.gz\")
sha256sums=('$SUM')

package() {
    cd \"\$srcdir/build\"
    install -Dm755 $PROJECT_BINARY \"\$pkgdir\"/usr/bin/$PROJECT_BINARY

    install -Dm644 README.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/README.md
    install -Dm644 LICENSE \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/LICENSE
    install -Dm644 CHANGELOG.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/CHANGELOG.md
}
" | tee PKGBUILD > /dev/null

makepkg --printsrcinfo > .SRCINFO
git add .
git commit -m "Release version $VERSION"
git push
